import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import Quill from 'quill'
// import 'quill/dist/quill.bubble.css'
import CodeMirror from 'codemirror'
import 'codemirror/mode/markdown/markdown'
import 'codemirror/lib/codemirror.css'
import styles from './Codemirror.module.styl'


const initialValue = `
#### Bienvenue

##### Pourquoi ce service ?

Ce service permet à chacun de **poster ou consulter les annonces et initiatives** liées à des **besoins
IMPORTANTS** d'entraide dans **VOTRE commune**, dans le cadre de la gestion du _COVID-19_.

##### Utilisation

Les documents étant accessibles à tous en écriture et en lecture, il est de la
responsabilité de chacun d'en faire bon usage et d'en maintenir la lisibilité
et la pertinence.

Il faut **penser ce document comme un INDEX, un SOMMAIRE** de ce qui existe
pour **AIGUILLER chacun vers une solution**. Il est préférable de ne pas détailler ici.

Modèle d'annonce à déposer et syntaxe associée (à copier/coller) :

    #### Annonces et Initiatives

    #### Modèles / Inspiration

    **Qui** : _auteur de l'annonce_  
    **Pour qui** : _à qui s'adresse cette annonce_  
    **Description** : _phrase courte en une ligne_  
    **Détails** : _site internet, numéro de téléphone, email, etc..._  


Les annonces pourront être **organisées par section ou thématique**.

Cette introduction étant faite, vous pouvez maintenant rédigez vos annonces dans la
**zone bleue** de la page.

##### Confidentialité

Ce document est en accès public, il est hébergé de pair à pair de manière décentralisée.  
Chacun de vos navigateurs enregistre ce qu'il voit et le transmet à ceux qui sont connectés au même document.  
Ne publiez pas d'informations sensibles.  
`

export default class Editor extends Component {
  constructor (props) {
    super(props)
    this.onRef = this.onRef.bind(this)
  }

  shouldComponentUpdate () {
    return false
  }

  onRef (ref) {
    this.editorEle = ref
  }

  componentDidMount () {
    const { onEditor, onChange } = this.props
    let editor

    if (this.editorEle) {
      // if (type === 'richtext') {
      //   editor = new Quill(ref, {
      //     theme: 'bubble'
      //   })

      //   editor.disable()
      // } else {
      // See: http://codemirror.net/doc/manual.html#config
      editor = CodeMirror(this.editorEle, {
        autofocus: true,
        inputStyle: 'contenteditable',
        lineNumbers: true,
        value: initialValue,
        viewportMargin: Infinity,
        lineWrapping: true,
        mode: 'markdown',
        readOnly: 'nocursor'
      })

      editor.on('change', () => {
        const editorVal = editor.getValue()
        if (onChange) onChange(editorVal === '' ? initialValue : editorVal, editor)
      })

      window.__peerPadEditor = editor
      // }
    }

    if (onEditor) onEditor(editor)
  }

  render () {
    return (
      <div className={styles.CodeMirrorContainer}>
        <div ref={this.onRef} />
      </div>
    )
  }
}

Editor.propTypes = {
  type: PropTypes.oneOf(['richtext', 'markdown', 'math']),
  editable: PropTypes.bool,
  onEditor: PropTypes.func,
  onChange: PropTypes.func
}

Editor.defaultProps = {
  editable: true
}
