const NODE_ENV = process.env.NODE_ENV

const isDev = NODE_ENV === 'development'

const defaultSwarmAddresses = {
  development: ['/ip4/0.0.0.0/tcp/9090/ws/p2p-websocket-star'],
  production: [
    '/dns4/rdv.alpha-01.bootstrap.stopcoronavirus.tech/tcp/443/wss/p2p-websocket-star'
  ]
}

const swarmAddresses = (process.env.WEBSOCKET_STAR ? process.env.WEBSOCKET_STAR.split(",") : undefined) ||
  defaultSwarmAddresses[NODE_ENV]

if (!swarmAddresses) {
  throw new Error(`Could not find default swarm addresses for ${NODE_ENV} NODE_ENV`)
}

module.exports = {
  peerStar: {
    ipfs: {
      swarm: swarmAddresses,
      bootstrap: isDev ? [] : 
      [
        '/dns4/ipfs-gateway-wss.alpha-01.bootstrap.stopcoronavirus.tech/tcp/443/wss/ipfs/QmXFeqdECoxEztvFfpJcTHj5zvBwfFFWEDVNu5uK5viSnb'
      ]
    },
    transport: {
      maxThrottleDelayMS: 1000
    }
  }
}
